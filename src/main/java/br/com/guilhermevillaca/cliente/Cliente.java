package br.com.guilhermevillaca.cliente;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Cliente {

	@Id
	private Integer id;
	private String nome;
	private String cpf;
	private Date datanascimento;
	@Transient
	private Integer idadeCalculada;

	public Cliente() {

	}

	public Cliente(Integer id, String nome, String cpf, Date datanascimento) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpf = cpf;
		this.datanascimento = datanascimento;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDatanascimento() {
		return datanascimento;
	}

	public void setDatanascimento(Date datanascimento) {
		this.datanascimento = datanascimento;
	}

	public Integer getIdadeCalculada() {
		LocalDate d = this.datanascimento.toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
		LocalDate hoje = new Date().toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
		return this.calculateAge(d, hoje);
	}

	public void setIdadeCalculada(Integer idadeCalculada) {
		this.idadeCalculada = idadeCalculada;
	}

	public Integer calculateAge(LocalDate birthDate, LocalDate currentDate) {
		return Period.between(birthDate, currentDate).getYears();
	}

}
