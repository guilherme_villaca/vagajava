package br.com.guilhermevillaca.cliente;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	public List<Cliente> list() {		
		List<Cliente> clientes = new ArrayList<>();
		clienteRepository.findAll().forEach(clientes::add);
		return clientes;
	}
	
	public Page<Cliente> search(int page, int size){
		Pageable pageRequest = PageRequest.of(
                page,
                size);
		return clienteRepository.findAll(pageRequest);
	}

	public Cliente get(Integer id) {		
		return clienteRepository.findById(id).orElse(new Cliente());		 
	}

	public void create(Cliente cliente) {
		clienteRepository.save(cliente);
	}

	public void update(Cliente cliente) {		
		clienteRepository.save(cliente);
	}

	public void delete(Integer id) {
		clienteRepository.deleteById(id);
	}

	public List<Cliente> get(String nome, String cpf) {
		List<Cliente> clientes = new ArrayList<>();
		clienteRepository.getClienteByNomeAndCpf(nome, cpf).forEach(clientes::add);
		return clientes;
	}

}
