package br.com.guilhermevillaca.cliente;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;

	@RequestMapping("/clientesList")
	public Page<Cliente> search(@RequestParam(
            value = "page",
            required = false,
            defaultValue = "0") int page,
    @RequestParam(
            value = "size",
            required = false,
            defaultValue = "10") int size) {
		return clienteService.search(page, size);
	}
	
	
	//getQueryString
	@RequestMapping(method=RequestMethod.GET, value="/clientes")
	public List<Cliente> get(@RequestParam(value="nome", required = false) String nome, @RequestParam(value="cpf", required = false) String cpf) {
		return clienteService.get(nome, cpf);		
	}
	@RequestMapping("/clientes/{id}")
	public Cliente get(@PathVariable Integer id) {
		return clienteService.get(id);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/clientes")
	public void create(@RequestBody Cliente cliente) {
		clienteService.create(cliente);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/clientes/{id}")
	public void update(@RequestBody Cliente cliente, @PathVariable Integer id) {
		clienteService.update(cliente);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/clientes/{id}")
	public void delete(@PathVariable Integer id) {
		clienteService.delete(id);
	}

}
