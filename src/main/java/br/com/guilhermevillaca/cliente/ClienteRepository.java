package br.com.guilhermevillaca.cliente;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	
	public List<Cliente> getClienteByNomeAndCpf(String nome, String cpf);
	
	Page<Cliente> findAll(Pageable pageable);
	
}
