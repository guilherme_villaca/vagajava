package br.com.guilhermevillaca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class TesteGuilhermeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteGuilhermeApplication.class, args);
	}

}
